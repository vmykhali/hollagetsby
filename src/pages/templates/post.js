import React from "react"
import { graphql, Link } from "gatsby"

import Layout from "../../components/layout"
import Img from "gatsby-image"
import SEO from "../../components/seo"

export const query = graphql`
  query($slug: String) {
    contentfulPost(slug: { eq: $slug }) {
      title
      publishedDate(formatString: "Do MMMM, YYYY")
      featureImage {
        fluid(maxWidth: 750) {
          ...GatsbyContentfulFluid
        }
      }
      excerpt {
        excerpt
      }
    }
  }
`

const Post = props => {
    return (
        <Layout>
            <SEO title={props.data.contentfulPost.title} />
            <Link to="/blog/">Visit the Blog Page</Link>
            <div className="content">
                <h1>{props.data.contentfulPost.title}</h1>
                <span className="meta">
          Posted on {props.data.contentfulPost.publishedDate}
        </span>

                {props.data.contentfulPost.featureImage && (
                    <Img
                        className="featured"
                        fluid={props.data.contentfulPost.featureImage.fluid}
                        alt={props.data.contentfulPost.title}
                    />
                )}
                <p className="text">{props.data.contentfulPost.excerpt.excerpt}</p>
            </div>
        </Layout>
    )
}

export default Post
