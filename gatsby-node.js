/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

// You can delete this file if you're not using it
const path = require("path")

exports.createPages = async ({ graphql, actions }) => {
    const { createPage } = actions;
    const response = await graphql(`
    query {
      allContentfulPost {
        edges {
          node {
            slug
          }
        }
      }
    }
  `)
    response.data.allContentfulPost.edges.forEach(edge => {
        console.log();
        createPage({
            path: `/blog/${edge.node.slug}`,
            component: path.resolve("./src/pages/templates/post.js"),
            context: {
                slug: edge.node.slug,
            },
        })
    })
}
